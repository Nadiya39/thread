# Add an ability to notify a user by email if anyone liked its post
1. Check if a registered user has received the email if anyone liked his/her post.
2. Verify the information from the received email (user, link).