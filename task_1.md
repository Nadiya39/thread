# Add an ability to dislike a post
1. A logged-in customer can dislike the post by clicking the icon `dislike`.
2. Check whether if disliked twice, dislike is getting canceled.
3. Verify that number of dislikes is getting increased once only.
4. Verify that number of dislikes is getting decreased once only.
5. A logged-in customer can not  have both like and dislike the same post from the same user.
6. Check that a logged-in customer will see the dislike notification if someone performs this action on the customer's post.
