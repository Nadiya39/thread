# Add an ability to update a comment
1. A logged-in customer can edit a comment by clicking the icon `update`.
2. A logged-in customer can update only his/her comments.
3. Check if other users can see the updated comments.
4. Check if the time when the comment will be updated is correct.
5. A logged-in customer can not save the updated comment with an empty message.
6. Check if the updated comment will be marked as `updated`.
7. A logged-in customer can not see the icon `update` on the other users’ comments.
8. Check whether the logic of updating comments is similar to adding comments (different types of media).
