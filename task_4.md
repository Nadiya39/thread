# Show posts which were liked by me
1. A logged-in customer can see posts that were liked by him/her by using a filter `liked by me`.
2. Check that this filter is only on the user's own page.
3. Check if other users can not see posts that are liked by a logged-in customer using the filter.
4. A logged-in customer can not see deleted posts that were liked before.
