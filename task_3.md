# Add an ability to delete a post (soft)
1. A logged-in customer can delete the post by clicking the icon `delete`.
2. A logged-in customer can delete only his/her own posts.
3. Check if other users can not see deleted posts.
4. Check if a logged-in customer can not see his/her own deleted posts.
5. Check if deleted posts will be saved in the database (soft deletion).
