# Add an ability to reset a password
1. A registered user can reset his/her own password.
2. Check if a registered user has received the email with the link (to reset a password).
3. Check if the new password is valid.
4. Check the ability to reset the password using the invalid email address.