# Thread

## The purpose of this document

Test scenarios to cover ToDo cards which are presented on the [Trello board](https://trello.com/b/9Y9ZIr6j).

## Test Scenarios

- [Add an ability to dislike a post.](task_1.md)
- [Add an ability to update a post.](task_2.md)
- [Add an ability to delete a post (soft).](task_3.md)
- [Show posts which were liked by me.](task_4.md)
- [Display who liked a post.](task_5.md)
- [Add an ability to update own profile (photo and username).](task_6.md)
- [Add an ability to add/show user status.](task_7.md)
- [Add an ability to reset a password.](task_8.md)
- [Add an ability to update a comment.](task_9.md)
- [Add an ability to delete a comment (soft)](task_10.md)
- [Add an ability to like a comment.](task_11.md)
- [Add an ability to dislike a comment.](task_12.md)
- [Add an ability to hide own posts in the common thread.](task_13.md)
- [Display who liked a comment.](task_14.md)
- [Add an ability to notify a user by email if anyone liked its post.](task_15.md)
- [Add an ability to share a post by email.](task_15.md)