# Display who liked a post
1. A logged-in customer can show a popup with a list of users that liked the post by using hovers over the `like` icon.
2. Check that the number of likes next to the icon matches the number of likes in the user list.
3. Check the display order of users who liked the post.
4. Check if users from the list are existing in the system.
5. A logged-in customer can see himself/herself in the list if he/she likes the post.