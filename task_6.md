# Add an ability to update own profile (photo and username)
1. A logged-in customer can update his/her own profile: photo and username.
2. Check if a logged-in customer uploads the photo with the correct size.
3. Check if a logged-in customer can edit the photo.
4. Check that the username of a logged-in customer is valid.
5. Check that the username of a logged-in customer is unique.
6. Check if other users can see the updated data after saving.
