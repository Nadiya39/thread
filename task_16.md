# Add an ability to share a post by email
1. A logged-in customer can share a post by clicking the icon `share` and putting the valid email address.
2. Check if a registered user has received the email if anyone shares a post by email.
3. Check the ability to share a post to multiple email addresses.
4. Check the ability to share a post to an invalid email address.
5. Check the ability to see the post from the link by the unregistered user.
6. Verify the information from the received email (user, link).