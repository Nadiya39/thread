# Add an ability to dislike a comment
1. A logged-in customer can dislike the comment by clicking the icon `like`.
2. Check whether if disliked twice, like is getting canceled.
3. Verify that number of dislikes is getting increased once only.
4. Verify that number of dislikes is getting decreased once only.
5. A logged-in customer can not have both like and dislike the same comment from the same user.