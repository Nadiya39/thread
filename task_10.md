# Add an ability to delete a comment (soft)
1. A logged-in customer can delete the comment by clicking the icon `delete`.
2. A logged-in customer can delete only his/her own comments.
3. Check if other users can not see deleted comments.
4. Check if a logged-in customer can not see his/her own deleted comments.
5. Check if deleted comments saved in the database (soft deletion).
