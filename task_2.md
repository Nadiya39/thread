# Add an ability to update a post
1. A logged-in customer can edit the post by clicking the icon `update`.
2. A logged-in customer can update only his/her own posts.
3. A logged-in customer can not save the updated post without any modifications.
4. Check if other users can see the updated post.
5. Check if the time when the post updated is correct
6. A logged-in customer can not save the updated post with an empty message.
7. Check if the updated post marked as `updated`.
8. A logged-in customer can not see the icon `update` on the other users’ posts.
9. Check whether the logic of updating posts is similar to adding posts (different types of media).
