# Add an ability to hide own posts in the common thread
1. A logged-in customer can hide his/her own posts in the common thread by using a filter.
2. Check whether if `hide my posts` is enabled, users can't turn `show only my posts`.
3. Check whether if `show only my posts` is enabled, users can't turn `hide my posts`.