# Add an ability to add/show user status
1. A logged-in customer can add his/her own user status.
2. A logged-in customer can see the statuses of other users.
3. Verify if the status input field is below the email input field on the profile page of a logged-in customer.
4. Verify if the user status is below a username when viewing comments of a post.
5. Verify if the user status is below a username in the page header of the user profile and is editable.
6. Check if the user status is valid.
