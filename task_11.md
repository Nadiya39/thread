# Add an ability to like a comment
1. A logged-in customer can like the comment by clicking the icon `like`.
2. Check whether if liked twice, like is getting canceled.
3. Verify that number of likes is getting increased once only.
4. Verify that number of likes is getting decreased once only.
5. A logged-in customer can not  have both like and dislike the same comment from the same user.
